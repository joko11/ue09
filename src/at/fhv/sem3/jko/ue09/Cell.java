/**
 * Description:
 * @author "jko"
 * @date Jan 3, 2018
 */
package at.fhv.sem3.jko.ue09;

public class Cell {

	private String _identifier;
	
	public Cell(String id) {
		_identifier = id;
	}
	
	public void setID(String id) {
		_identifier = id;
	}
	
	public String getID() {
		return _identifier;
	}
	
	@Override
	public String toString() {
		return _identifier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_identifier == null) ? 0 : _identifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (_identifier == null) {
			if (other._identifier != null)
				return false;
		} else if (!_identifier.equals(other._identifier))
			return false;
		return true;
	}
}
