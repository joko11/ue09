/**
 * Description:
 * @author "jko"
 * @date Jan 13, 2018
 */
package at.fhv.sem3.jko.ue09;

import java.util.ArrayList;
import java.util.List;

import at.fhv.sem3.jko.ue09.TableView.CostComparison;

public class Cost {

	private static Cost _context = new Cost();
	private List<CostComparison> _costs = new ArrayList<>();
	
	public static Cost getInstance() {
		return _context;
	}
	
	public List<CostComparison> getCosts(){
		return _costs;
	}
	
	public void setCosts(List<CostComparison> costs) {
		_costs = costs;
	}
	
}
