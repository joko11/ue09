/**
 * Description:
 * @author "jko"
 * @date Jan 3, 2018
 */
package at.fhv.sem3.jko.ue09;

import java.util.Random;

public enum Direction {

	UP(0),
	DOWN(1),
	LEFT(2),
	RIGHT(3);

	private static final int _NUMBEROFDIRECTIONS = 4;
	private int _numericValue;
	
	private Direction(int numericValue) {
		_numericValue = numericValue;
	}
	
	public Direction getOppositeDirection() {
		Direction result = null;
		switch(this) {
			case UP:
				result = DOWN;
				break;
			case DOWN:
				result = UP;
				break;
			case RIGHT:
				result = LEFT;
				break;
			case LEFT:
				result = RIGHT;
				break;
			default:
				throw new IllegalArgumentException();
				//TODO: Checked exception
		}
		return result;
	}
	
	private static Direction getDirectionByValue(int value) {
		Direction result = null;
		for(Direction dir : values()) {
			if(value == dir._numericValue) {
				result =  dir;
			}
		}
		return result;
	}
	
	public static Direction getRandom() {
		Random r = new Random();
		int nounce = r.nextInt(_NUMBEROFDIRECTIONS);
		return getDirectionByValue(nounce);
	}
}
