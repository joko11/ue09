/**
 * Description: This class represents a EightPuzzle.
 * @author "jko"
 * @date Jan 3, 2018
 */
package at.fhv.sem3.jko.ue09;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EightPuzzle {

	private static final char[] _LABELS = {'1','2','3','4','5','6','7','8','X'};
	private static final int _DIMENSION = 3;
	private EightPuzzle _parent;
	private Cell[][] _state;
	private int _holeRow;
	private int _holeCol;
	private Direction _reachedFrom;
	private int _currentWeight;
	
	/**
	 * Initializes an EightPuzzle with a standard, solved configuration.
	 */
	public EightPuzzle() {
		_state = new Cell[_DIMENSION][_DIMENSION];
		int labelCount = 0;
		for(int row = 0; row < _DIMENSION; row ++) {
			for(int col = 0; col < _DIMENSION; col++) {
				_state[row][col] = new Cell(String.valueOf(_LABELS[labelCount++]));
			}
		}
		_state[_DIMENSION - 1][_DIMENSION - 1].setID("X");
		_holeRow = _DIMENSION - 1;
		_holeCol = _DIMENSION - 1;
		_currentWeight = Integer.MAX_VALUE;
	}

	/**
	 * Initializes an EightPuzzle based on a provided EightPuzzle. Copy constructor
	 * 
	 * @param puzzle The provided EightPuzzle
	 */
	public EightPuzzle(EightPuzzle puzzle) {
		this(puzzle, puzzle._reachedFrom);
	}

	/**
	 * Initializes an EightPuzzle based on a provided EightPuzzle and with a direction from
	 * which the {@link #_state} is reached from.
	 * 
	 * @param puzzle The provided EightPuzzle
	 * @param dir The specified direction.
	 */
	public EightPuzzle(EightPuzzle puzzle, Direction dir) {
		_state = new Cell[_DIMENSION][_DIMENSION];
		for(int row = 0 ; row < _DIMENSION; row++) {
			for(int col = 0; col < _DIMENSION; col++) {
				_state[row][col] = new Cell(puzzle._state[row][col].getID());
			}
		}
		_holeRow = puzzle._holeRow;
		_holeCol = puzzle._holeCol;
		_reachedFrom = dir;
		_currentWeight = Integer.MAX_VALUE;
	}
	
	/**
	 * Checks if this puzzle is already solved.
	 * 
	 * @return True if puzzle is solved, else false.
	 */
	public boolean isSolved() {
		boolean result = true;
		int count = 0;
		
		for(int row = 0; row < _DIMENSION; row++) {
			for(int col = 0; col < _DIMENSION; col++) {
				if(!(_state[row][col].getID().equals(String.valueOf(_LABELS[count])))) {
					result = false;
				}
				count++;
			}
		}
		return result;
	}
	
	/**
	 * Moves the "empty" position into the specified direction.
	 * 
	 * @param dir The specified direction
	 * @return True if move successful, else false.
	 */
	public boolean move(Direction dir) {
		return performMove(dir);
	}
	
	private boolean performMove(Direction dir) {
		boolean result = false;
		Cell temp = _state[_holeRow][_holeCol];
		
		switch(dir) {
			case UP:
				if(_holeRow > 0) {
					_state[_holeRow][_holeCol] = _state[_holeRow - 1][_holeCol];
					_holeRow--;
					_state[_holeRow][_holeCol] = temp;
					result = true;
				}
				break;	
			case DOWN:
				if(_holeRow < _DIMENSION - 1) {
					_state[_holeRow][_holeCol] = _state[_holeRow + 1][_holeCol];
					_holeRow++;
					_state[_holeRow][_holeCol] = temp;
					result = true;
				}
				break;
			case RIGHT:
				if(_holeCol < _DIMENSION - 1) {
					_state[_holeRow][_holeCol] = _state[_holeRow][_holeCol + 1];
					_holeCol++;
					_state[_holeRow][_holeCol] = temp;
					result = true;
				}
				break;
			case LEFT:
				if(_holeCol > 0) {
					_state[_holeRow][_holeCol] = _state[_holeRow][_holeCol - 1];
					_holeCol--;
					_state[_holeRow][_holeCol] = temp;
					result = true;
				}
				break;
		}
		return result;
	}
	
	/**
	 * Provides the follow up states of the current puzzle.
	 * 
	 * @return An Iterable<EightPuzzle> of the follow up states.
	 */
	public Iterable<EightPuzzle> followUpStates(){
		EightPuzzle current = new EightPuzzle(this);
		
		List<EightPuzzle> states = new ArrayList<>();
		for(Direction dir : Direction.values()) {
			if(performMove(dir)) {
				if(_reachedFrom == null || dir != _reachedFrom.getOppositeDirection()) {
					states.add(new EightPuzzle(this, dir));
				}
			}
			_state = copyArray(current._state);
			_holeRow = current._holeRow;
			_holeCol = current._holeCol;
		}
		return states;
	}
	
	/**
	 * Prints the current puzzle.
	 */
	public void print() {
		System.out.println(toString());
	}
	
	private Cell[][] copyArray(Cell[][] cells){
		Cell[][] copy = new Cell[_DIMENSION][_DIMENSION];
		for(int row = 0; row < _DIMENSION; row++) {
			for(int col = 0 ; col < _DIMENSION; col++) {
				copy[row][col] = cells[row][col];
			}
		}
		return copy;
	}
	
	/**
	 * Returns the current {@link #_currentWeight} of this puzzle.
	 * 
	 * @return The current {@link #_currentWeight}.
	 */
	public int getCurrentWeight() {
		return _currentWeight;
	}

	/**
	 * Sets the current {@link #_currentWeight} of this puzzle.
	 * 
	 * @param currentWeight the specified weight.
	 */
	public void setCurrentWeight(int currentWeight) {
		_currentWeight = currentWeight;
	}
	
	/**
	 * Calculates and return the h1 heuristic value.
	 * 
	 * @return The h1 heuristic value of this puzzle.
	 */
	public int h1() {
		int wrongPlaced = 0;
		int labelIndex = 0;
		for(int row = 0; row < _DIMENSION; row++) {
			for(int col = 0 ; col < _DIMENSION; col++) {
				if(!_state[row][col].getID().equals(String.valueOf(_LABELS[labelIndex]))) {
					wrongPlaced++;
				}
				labelIndex++;
			}
		}
		return wrongPlaced;
	}

	/**
	 * Calculates and return the h2 heuristic value.
	 * 
	 * @return The h2 heuristic value of this puzzle.
	 */
	public int h2() {
		int manhattanDistance = 0;
		int labelIndex = 0;
		for(int row = 0; row < _DIMENSION; row++) {
			for(int col = 0; col < _DIMENSION; col++) {
				if(!_state[row][col].getID().equals(String.valueOf(_LABELS[labelIndex]))) {
					int index;
					String currentID = _state[row][col].getID();
					if(currentID.equals("X")) {
						index = (_DIMENSION * _DIMENSION);
					}else {
						index = Integer.parseInt(currentID);
					}
					manhattanDistance += calcManhattanDistance(row, col, index);
				}
				labelIndex++;
			}
		}
		return manhattanDistance;
	}
	
	private int calcManhattanDistance(int row, int col, int index) {
		index--;
		int targetRow = index/_DIMENSION;
		int targetCol = index % _DIMENSION;
		return (Math.abs(row - targetRow) + Math.abs(col - targetCol));
	}

	/**
	 * Returns the {@link #_parent} of this EightPuzzle.
	 * 
	 * @return the {@link #_parent}
	 */
	public EightPuzzle getParent() {
		return _parent;
	}

	/**
	 * Sets the current {@link #_parent} of this puzzle.
	 * 
	 * @param parent the specified parent.
	 */
	public void setParent(EightPuzzle parent) {
		_parent = parent;
	}

	@Override
	public String toString() {
		StringBuilder strBuild = new StringBuilder();
		if(_reachedFrom == null) {
			strBuild.append("Initial State: \n");
		} else {
			strBuild.append("reached by shifting hole: " + _reachedFrom + "\n");
		}
		for(int row = 0; row < _DIMENSION; row ++) {
			for(int col = 0; col < _DIMENSION; col++) {
				strBuild.append(_state[row][col] + "\t");
			}
			strBuild.append("\n");
		}
		strBuild.append("\n");
		if(isSolved()) {
			strBuild.append("Solved!");
		}
		return strBuild.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + _currentWeight;
		result = prime * result + _holeCol;
		result = prime * result + _holeRow;
		result = prime * result + ((_reachedFrom == null) ? 0 : _reachedFrom.hashCode());
		result = prime * result + Arrays.deepHashCode(_state);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EightPuzzle other = (EightPuzzle) obj;
		if (_currentWeight != other._currentWeight)
			return false;
		if (_holeCol != other._holeCol)
			return false;
		if (_holeRow != other._holeRow)
			return false;
		if (_reachedFrom != other._reachedFrom)
			return false;
		if (!Arrays.deepEquals(_state, other._state))
			return false;
		return true;
	}
}
