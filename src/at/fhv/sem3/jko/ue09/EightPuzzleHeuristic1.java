/**
 * Description: This class represents a Comparator for the h1 heuristic.
 * @author "jko"
 * @date Jan 10, 2018
 */
package at.fhv.sem3.jko.ue09;

import java.util.Comparator;

public class EightPuzzleHeuristic1 implements IHeuristic<EightPuzzle>, Comparator<EightPuzzle>  {

	/* (non-Javadoc)
	 * @see at.fhv.sem3.jko.ue09.IHeuristic#getHeuristicValue()
	 */
	@Override
	public int getHeuristicValue(EightPuzzle ep) {
		return ep.getCurrentWeight() + ep.h1();
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(EightPuzzle ep1, EightPuzzle ep2) {
		return Integer.compare((ep1.getCurrentWeight() + ep1.h1()), (ep2.getCurrentWeight() + ep2.h1()));
	}
}
