/**
 * Description: This class represents a Comparator for the h2 heuristic.
 * @author "jko"
 * @date Jan 10, 2018
 */
package at.fhv.sem3.jko.ue09;

public class EightPuzzleHeuristic2 implements IHeuristic<EightPuzzle> {


	/* (non-Javadoc)
	 * @see at.fhv.sem3.jko.ue09.IHeuristic#getHeuristicValue(java.lang.Object)
	 */
	@Override
	public int getHeuristicValue(EightPuzzle ep) {
		return ep.getCurrentWeight() + ep.h2();
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(EightPuzzle ep1, EightPuzzle ep2) {
		return Integer.compare((ep1.getCurrentWeight() + ep1.h2()), (ep2.getCurrentWeight() + ep2.h2()));
	}
}
