/**
 * Description: This class represents a EightPuzzleSolver which solves EightPuzzles.
 * @author "jko"
 * @date Jan 4, 2018
 */
package at.fhv.sem3.jko.ue09;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import at.fhv.sem3.jko.ue09.TableView.CostComparison;

public class EightPuzzleSolver {

	private EightPuzzle _puzzle;
	private int _nodeCount;
	private int _depth;
	private LinkedList<EightPuzzle> _solution;
	private IHeuristic<EightPuzzle> _heuristic;
	private Viewable _view;
	
	/**
	 * Initializes a EightPuzzleSolver with a specified EightPuzzle.
	 * 
	 * @param puzzle The specified EightPuzzle.
	 */
	public EightPuzzleSolver(EightPuzzle puzzle) {
		this(puzzle, new EightPuzzleHeuristic2(), new GuiView());
	}
	
	/**
	 * Initializes a EightPuzzleSolver with a specified EightPuzzle and a view.
	 * 
	 * @param puzzle The specified EightPuzzle.
	 * @param view The specified view.
	 */
	public EightPuzzleSolver(EightPuzzle puzzle, Viewable view) {
		_view = view;
		_puzzle = puzzle;
		_solution = new LinkedList<>();
	}
	
	/**
	 * Initializes a EightPuzzleSolver with a specified EightPuzzle, a heuristic and a view.
	 * 
	 * @param puzzle The specified EightPuzzle.
	 * @param heuristic The specified heuristic.
	 * @param view The specified view.
	 */
	public EightPuzzleSolver(EightPuzzle puzzle, IHeuristic<EightPuzzle> heuristic, Viewable view) {
		_puzzle = puzzle;
		_solution = new LinkedList<>();
		_heuristic = heuristic;
		_view = view;
	}
	
	/**
	 * Solves the specified puzzle per IDS algorithm.
	 */
	public void solveIDS(){
		reset();
		EightPuzzle current = new EightPuzzle(_puzzle);
		if(!current.isSolved()) {
			for(int limit = 1; limit < 100; limit++) {
				EightPuzzle found = ids(current, limit);
				if(found != null) {
					_solution.addFirst(_puzzle);
					_depth = limit;
					return;
				}
			}
		}
	}
	
	private EightPuzzle ids(EightPuzzle puzzle, int limit) {
		if(limit == 0 && puzzle.isSolved()) {
			return puzzle;
		}
		if(limit > 0) {
			for(EightPuzzle ep : puzzle.followUpStates()) {
				EightPuzzle current = ids(ep, limit - 1);
				_nodeCount++;
				if(current != null) {
					_solution.addFirst(ep);
					return current;
				}
			}
		}
		return null;
	}
	
	/**
	 * Solves the current EightPuzzle with the former specified heuristic.
	 * 
	 * @throws HeuristicNotSpecifiedException If no heuristic specified.
	 */
	public void solveWithHeuristic() throws HeuristicNotSpecifiedException{
		reset();
		if(_heuristic == null) {
			throw new HeuristicNotSpecifiedException("No Heuristic specified");
		}
			EightPuzzle current = new EightPuzzle(_puzzle);
			solveHeuristic(current);
	}
	
	/**
	 * Solves the current EightPuzzle with the specified heuristic.
	 * 
	 * @param heuristic The specified heuristic.
	 */
	public void solveWithHeuristic(IHeuristic<EightPuzzle> heuristic) {
		reset();
		_heuristic = heuristic;
		EightPuzzle current = new EightPuzzle(_puzzle);
		solveHeuristic(current);
	}
	
	/**
	 * Sets the {@link #_heuristic}.
	 * 
	 * @param heuristic The specified heuristic.
	 */
	public void setHeuristic(IHeuristic<EightPuzzle> heuristic) {
		_heuristic = heuristic;
	}
	
	private void solveHeuristic(EightPuzzle ep) {		
		Queue<EightPuzzle> openList = new PriorityQueue<>(_heuristic);
		Set<EightPuzzle> closedList = new HashSet<>();
		ep.setCurrentWeight(0);
		ep.setParent(null);
		openList.add(ep);
		
		EightPuzzle current = null;
		while(!openList.isEmpty()) {
			current  = openList.poll();
			closedList.add(current);
			_nodeCount++;
			
			if(current.isSolved()) {
				initSolution(current);
				return;
			}
			for(EightPuzzle e : current.followUpStates()) {
				if(!closedList.contains(e)) {
					int cost = current.getCurrentWeight() + 1;
					if(cost < e.getCurrentWeight()) {
						e.setCurrentWeight(cost);
						e.setParent(current);
						openList.add(e);
					}
				}
			}
		}
	}
	
	private void initSolution(EightPuzzle current) {
		_solution.addFirst(current);
		while((current = current.getParent()) != null){
			_solution.addFirst(current);
		}
		_depth = _solution.size() - 1;
	}
	
	private void reset() {
		_solution.clear();
		_depth = 0;
		_nodeCount = 0;
	}
	
	/**
	 * Return the solution path.
	 * 
	 * @return A List containing the steps of the solution of the EightPuzzle.
	 */
	public List<EightPuzzle> getSolutionTree(){
		return Collections.unmodifiableList(_solution);
	}
	
	/**
	 * Returns the number of nodes.
	 * 
	 * @return {@link #_nodeCount}
	 */
	public int getNodeCount() {
		return _nodeCount;
	}
	
	/**
	 * Return the depth of the solution.
	 * 
	 * @return {@link #_depth}
	 */
	public int getDepth() {
		return _depth;
	}
	
	/**
	 * Display a comparison-statistic.
	 * 
	 * @param numberOfComparison The number of comparisons.
	 */
	public void displayComparisonTable(int numberOfComparison) {
		Cost cost = Cost.getInstance();
		cost.setCosts(getStats(numberOfComparison));
		_view.display(cost);
	}
	
	/**
	 * Provides a List of CostComparison items representing a statistic.
	 * 
	 * @param numberOfComparisons The number of comparisons.
	 * @return List of CostComparison items.
	 */
	public List<CostComparison> getStats(int numberOfComparisons){
		List<CostComparison> comparisons = new ArrayList<>();
		
		while(numberOfComparisons > 0) {
			CostComparison comp = new CostComparison();
			createRandomPuzzle();
			
			solveIDS();
			int idsNodes = _nodeCount;
			double idsBF = calculateEffectiveBranchingFactor();
			
			solveWithHeuristic(new EightPuzzleHeuristic1());
			int h1Nodes = _nodeCount;
			double h1BF = calculateEffectiveBranchingFactor();
			
			solveWithHeuristic(new EightPuzzleHeuristic2());
			int h2Nodes = _nodeCount;
			double h2BF = calculateEffectiveBranchingFactor();
			
			comp.setDepth(String.valueOf(_depth));
			
			comp.setIdsNodes(String.valueOf(idsNodes));
			comp.setH1Nodes(String.valueOf(h1Nodes));
			comp.setH2Nodes(String.valueOf(h2Nodes));
			
			comp.setIdsBF(String.format("%.3f", idsBF));
			comp.setH1BF(String.format("%.3f", h1BF));
			comp.setH2BF(String.format("%.3f", h2BF));
			
			comparisons.add(comp);
			numberOfComparisons--;
		}
		return comparisons;
	}
	
	/**
	 * Calculates the effective branching factor.
	 * 
	 * @return The effective branching factor.
	 */
	public double calculateEffectiveBranchingFactor() {
		double res = 0;
		double eps = 0.0001;
		
		double branchingFactor = 1.00;
		
		do {			
			branchingFactor += eps;
			res = -_nodeCount + (branchingFactor*_nodeCount) + 1 - (Math.pow(branchingFactor, _depth+1));
		}while(res > eps);
		
		return branchingFactor;
	}
	
	/**
	 * Sets the {@link #_puzzle} to a random configuration.
	 */
	public void createRandomPuzzle() {
		_puzzle = new EightPuzzle();
		int count = 0;
		while(count < 50) {
			if(_puzzle.move(Direction.getRandom())) {
				count++;
			}
		}
	}
}
