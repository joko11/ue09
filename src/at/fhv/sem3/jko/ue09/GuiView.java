/**
 * Description: Provides a Gui-View.
 * @author "jko"
 * @date Jan 13, 2018
 */
package at.fhv.sem3.jko.ue09;

import javafx.application.Application;

public class GuiView implements Viewable {

	/* (non-Javadoc)
	 * @see at.fhv.sem3.jko.ue09.Viewable#display(at.fhv.sem3.jko.ue09.EightPuzzleSolver)
	 */
	@Override
	public void display(Cost cost) {
		Application.launch(at.fhv.sem3.jko.ue09.TableView.FMXLTableView.class);
	}
}
