/**
 * Description:
 * @author "jko"
 * @date Jan 10, 2018
 */
package at.fhv.sem3.jko.ue09;

import java.util.Comparator;

public interface IHeuristic<T> extends Comparator<T>{

	public int getHeuristicValue(T t);
}
