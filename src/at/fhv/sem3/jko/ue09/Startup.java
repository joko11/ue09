/**
 * Description:
 * @author "jko"
 * @date Jan 3, 2018
 */
package at.fhv.sem3.jko.ue09;

import at.fhv.sem3.jko.ue09.TableView.ConsoleView;


public class Startup {

	public static void main(String[] args){
		
		EightPuzzle puzzle = new EightPuzzle();	
		EightPuzzleSolver eps = new EightPuzzleSolver(puzzle, new GuiView());

		eps.displayComparisonTable(10);
	}
	
	public void proofH2NotAlwaysBetter() {
		EightPuzzle puzzle = new EightPuzzle();	
		puzzle.move(Direction.LEFT);
		puzzle.move(Direction.LEFT);
		puzzle.move(Direction.UP);
		puzzle.move(Direction.RIGHT);
		puzzle.move(Direction.RIGHT);

		EightPuzzleSolver gs = new EightPuzzleSolver(puzzle, new ConsoleView());
		gs.solveWithHeuristic(new EightPuzzleHeuristic1());
		System.out.println(gs.getDepth());
		System.out.println(gs.getNodeCount());
		System.out.println(gs.getSolutionTree());

		System.out.println();
		System.out.println();
			
		gs.solveWithHeuristic(new EightPuzzleHeuristic2());
		System.out.println(gs.getDepth());
		System.out.println(gs.getNodeCount());
		System.out.println(gs.getSolutionTree());
	}

	
	
	public void oldStuff() {
//		puzzle.move(Direction.UP);
//		puzzle.move(Direction.DOWN);
//		puzzle.move(Direction.LEFT);
//		puzzle.move(Direction.LEFT);
//		puzzle.move(Direction.UP);		
//		puzzle.move(Direction.RIGHT);
//		puzzle.move(Direction.DOWN);
//		puzzle.move(Direction.RIGHT);
//		puzzle.move(Direction.UP);
//		puzzle.move(Direction.DOWN);
//		puzzle.move(Direction.LEFT);
//		puzzle.move(Direction.LEFT);
//		puzzle.move(Direction.UP);
//		puzzle.move(Direction.UP);
//		puzzle.move(Direction.RIGHT);
//		puzzle.move(Direction.DOWN);
//		puzzle.move(Direction.RIGHT);
//		puzzle.move(Direction.UP);
//		puzzle.move(Direction.LEFT);

//		puzzle.move(Direction.UP);
//		puzzle.move(Direction.LEFT);
//		puzzle.move(Direction.UP);
//		puzzle.move(Direction.LEFT);


//		Application.launch(at.fhv.sem3.jko.ue09.TableView.FMXLTableView.class, "");
//		
//		for(int i = 0; i < 100; i++) {
//			puzzle.move(Direction.getRandom());
//		}
	}
}
