/**
 * Description:
 * @author "jko"
 * @date Jan 13, 2018
 */
package at.fhv.sem3.jko.ue09.TableView;

import at.fhv.sem3.jko.ue09.Cost;
import at.fhv.sem3.jko.ue09.Viewable;

public class ConsoleView implements Viewable{

	/* (non-Javadoc)
	 * @see at.fhv.sem3.jko.ue09.Viewable#display()
	 */
	@Override
	public void display(Cost cost) {

		for(CostComparison costComparison : cost.getCosts()) {
		
		System.out.println("Cost-Statistic: Depth: " + costComparison.getDepth() + "\t | \t IDS \t | \t H1 \t | \tH2");
		System.out.println("Visited Nodes:  \t\t | \t" + costComparison.getIdsNodes() + " \t | \t" 
							+ costComparison.getH1Nodes() + "\t | \t" + costComparison.getH2Nodes());
		System.out.println("Branching-Factor \t\t | \t " + costComparison.getIdsBF() + "\t | \t" 
							+ costComparison.getH1BF() + "\t | \t" + costComparison.getH2BF());
		System.out.println();
		}
	}
}
