/**
 * Description:
 * @author "jko"
 * @date Jan 13, 2018
 */
package at.fhv.sem3.jko.ue09.TableView;

import java.util.Comparator;

import javafx.beans.property.SimpleStringProperty;

public class CostComparison {

	private final SimpleStringProperty depth = new SimpleStringProperty("");
	private final SimpleStringProperty idsNodes = new SimpleStringProperty("");
	private final SimpleStringProperty h1Nodes = new SimpleStringProperty("");
	private final SimpleStringProperty h2Nodes = new SimpleStringProperty("");
	private final SimpleStringProperty idsBF = new SimpleStringProperty("");
	private final SimpleStringProperty h1BF = new SimpleStringProperty("");
	private final SimpleStringProperty h2BF = new SimpleStringProperty("");
	
	private static class CostDepthComparator implements Comparator<CostComparison>{

		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(CostComparison o1, CostComparison o2) {
			return Integer.compare(Integer.parseInt(o1.getDepth()), Integer.parseInt(o2.getDepth()));
		}
	}
	
	public CostComparison() {
		 this("","","","","","","");
	};
	public CostComparison(String depth, String idsNodes, String h1Nodes, String h2Nodes, String idsBF, String h1BF, String h2BF) {
		setDepth(depth);
		
		setIdsNodes(idsNodes);
		setIdsBF(idsBF);
		
		setH1Nodes(h1Nodes);
		setH1BF(h1BF);
		
		setH2Nodes(h2Nodes);
		setH2BF(h2BF);
	}
	public String getDepth() {
		return depth.get();
	}
	public String getIdsNodes() {
		return idsNodes.get();
	}
	public String getH1Nodes() {
		return h1Nodes.get();
	}
	public String getH2Nodes() {
		return h2Nodes.get();
	}
	public String getIdsBF() {
		return idsBF.get();
	}
	public String getH1BF() {
		return h1BF.get();
	}
	public String getH2BF() {
		return h2BF.get();
	}
	public void setIdsNodes(String iNodes) {
		idsNodes.set(iNodes);
	}
	public void setH1Nodes(String h1Nds) {
		h1Nodes.set(h1Nds);
	}
	public void setH2Nodes(String h2Nds) {
		h2Nodes.set(h2Nds);
	}
	public void setDepth(String dpth) {
		depth.set(dpth);
	}
	public void setIdsBF(String iBF) {
		idsBF.set(iBF);
	}	
	public void setH1BF(String h1BrFa) {
		h1BF.set(h1BrFa);
	}
	public void setH2BF(String h2BrFa) {
		h2BF.set(h2BrFa);
	}
	
	public static Comparator<CostComparison> getDepthComparator(){
		return new CostDepthComparator();
	}
}	
