/**
 * Description:
 * @author "jko"
 * @date Jan 13, 2018
 */
package at.fhv.sem3.jko.ue09.TableView;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class FMXLTableView extends Application{

	private static FXMLTableViewController _controller;
	
    public static void main(String[] args) {
        launch();
    }
	
	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("TableView.fxml"));
		stage.setTitle("Cost-Comparison: IDS | A*");
		Pane root = loader.load();
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.getIcons().add(new Image("icon.png"));
		stage.setResizable(false);
		stage.show();
	}

}
