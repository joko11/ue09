/**
 * Description:
 * @author "jko"
 * @date Jan 13, 2018
 */
package at.fhv.sem3.jko.ue09.TableView;

import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;

import at.fhv.sem3.jko.ue09.Cost;
import at.fhv.sem3.jko.ue09.EightPuzzle;
import at.fhv.sem3.jko.ue09.EightPuzzleSolver;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.util.converter.IntegerStringConverter;

public class FXMLTableViewController implements Initializable {

	@FXML
	private TableView<CostComparison> tableView;
	@FXML
	private Button but1;
	@FXML
	private TextField tf1;

	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
			
		UnaryOperator<Change> integerFilter = change -> {
		    String newText = change.getControlNewText();
		    if (newText.matches("(^$)|0|[1-9]|10|11|12|13|14|15")) { 
		        return change;
		    }
		    return null;
		};
		
		tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
		List<CostComparison> comparisons = Cost.getInstance().getCosts();
		comparisons.sort(CostComparison.getDepthComparator());
		for(CostComparison costComparison : comparisons) {
			tableView.getItems().add(costComparison);
		}
		
		tf1.setTextFormatter(new TextFormatter<java.lang.Integer>(new IntegerStringConverter(), 0, integerFilter));
		tf1.setText("5");
	}
	
	@FXML
	public void calculateComparisons() {
		int numberOfComparisons = 0;
		tableView.getItems().clear();
		if(tf1.getText() != null) {
			numberOfComparisons = Integer.parseInt(tf1.getText());
		}
		EightPuzzleSolver eps = new EightPuzzleSolver(new EightPuzzle());
		List<CostComparison> comparisons = eps.getStats(numberOfComparisons);
		comparisons.sort(CostComparison.getDepthComparator());
		
		ObservableList<CostComparison> list = tableView.getItems();
		list.addAll(comparisons);
	
	}
}
