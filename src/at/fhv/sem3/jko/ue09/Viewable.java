/**
 * Description:
 * @author "jko"
 * @date Jan 13, 2018
 */
package at.fhv.sem3.jko.ue09;

public interface Viewable {

	public void display(Cost cost);
}
